FROM adoptopenjdk/openjdk11:jdk-11.0.9.1_1

EXPOSE 8888

ENV APP_USER="app"
ENV MAVEN_CLI_OPTS: "-s .m2/settings.xml --batch-mode"
ENV MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

MAINTAINER nelsongomes.com

#In Progress
#COPY checkstyle.xml /tmp/
#COPY pom.xml /tmp/
#COPY src /tmp/src/
#WORKDIR /tmp/
#RUN mvn clean package -Dmaven.test.skip=true -DskipTests -Djacoco.skip=true

COPY target/configuration.server.jar configuration.server.jar

ENTRYPOINT ["java","-jar","/configuration.server.jar"]
